import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/lista",
      name: "lista",
      component: () => import("./components/lista")
    },
    {
      path: "/dettaglio/:id",
      name: "dettaglio",
      component: () => import("./components/dettaglio")
    },
    {
      path: "/aggiungi",
      name: "aggiungi",
      component: () => import("./components/aggiungi")
    }
  ]
});
