import http from "../http-common";
/**
 * Servizi per gestione degli event:
 * ho dovuto non usare il Rest Servizio perchè incompleto degli aggiornamenti
 * dei dati
 * 
 * ho preferito gestire tutto da localstorage...log
 */
class FormService {
//metodo per recuperare tutti gli elementi e risultati del serviziio Rest
  getAll() {
    return http.get("/posts");
  }

//metodo per recuperare un elemento nel localstorage a partire del suo id  
  get(id) {
    //return http.get(`/posts/${id}`);
    console.log(id);
    var datas = JSON.parse(localStorage.getItem('datas')) || [];
    console.log(datas[0])
    for(var indx = 0; indx <datas.length; indx++){
      if(id == datas[indx].id){
          return datas[indx];
      } 
    }
  }

  //metodo per creare gli elementi
  create(data) {
    return http.post("/posts", data);
  }

  //metodo per aggiornare gli elementi
  update(id, data) {
    return http.put(`/posts/${id}`, data);
  }
//metodo per aggiornare gli elementi nel localstorage
  updateInLocalStorage(id, data) {
    
    var items = [];
    var datas = JSON.parse(localStorage.getItem('datas')) || [];
    for(var indx = 0; indx <datas.length; indx++){
      if(id == datas[indx].id){
        console.log(datas[id])
        datas[indx].title = data.title;
        datas[indx].body = data.body;
        data = datas[indx];
      } 
      items.push(datas[indx]);
    }
    localStorage.setItem("datas", JSON.stringify(items));
    return data;
  }

  //Cancellare un elemento dall'id con la chiamata Rest
  delete(id) {
    return http.delete(`/posts/${id}`);
  }


  //Cancella elementi nel localstorage per id
  deleteItem(id) {
    var items = [];
    var datas = JSON.parse(localStorage.getItem('datas')) || [];
    for(var indx = 0; indx <datas.length; indx++){
      if(id != datas[indx].id){
        console.log(datas[id])
        items.push(datas[indx]);
      }  
    }
    localStorage.setItem("datas", JSON.stringify(items));
  }
//Cancella tutti gli elementi con la chiamata Rest
  deleteAll() {
    return http.delete(`/posts`);
  }
//Cerca l'elemento dal titolo con la chiamata Rest
  findByTitolo(titolo) {
    return http.patch(`/posts/1?title=${titolo}`);
  }

  //Ricerca l'elemento nel localstorage per il titolo
  ricercaByTitolo(value){
    
    var result = [];
    var datas = JSON.parse(localStorage.getItem('datas')) || [];
    if(value){
      for(var indx = 0; indx <datas.length; indx++){
        if(datas[indx].title.includes(value)){
          result.push(datas[indx]);
        } 
      }
    } 
    return (result && result.length > 0) ? result: datas;
  } 
 
  //aggiorna gli elementi nel localstorage
  aggiornaLocalStorage(data, listaOriginale){
    
    var newDatas = JSON.parse(localStorage.getItem('datas')) || [];
    if(listaOriginale){
      var listaOriginaleItems = JSON.parse(localStorage.getItem('LISTA_ORIGINALE')) || [];
      if(listaOriginaleItems.length == 0){
        /*newDatas.push(data);
        localStorage.setItem("datas", JSON.stringify(newDatas));
        localStorage.setItem("LISTA_ORIGINALE", JSON.stringify(listaOriginaleItems));
        */
      }
    } else {
      newDatas.push(data);
      localStorage.setItem("datas", JSON.stringify(newDatas));
    }
    console.log(newDatas);
  }
  
  //pulire il localstorage
  cleanAllElement(){
    localStorage.setItem("datas", JSON.stringify([]));
    localStorage.setItem("LISTA_ORIGINALE", JSON.stringify([]));
  }
}

export default new FormService();
