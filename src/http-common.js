import axios from "axios";

/*AXIOS -> ho usato axio, libreria che mi permette la gestione delle chiamate Rest */
export default axios.create({
  //Radice del Url dei webservices
  baseURL: "https://jsonplaceholder.typicode.com",
  headers: {
    "Content-type": "application/json"
  }
});
